import React from 'react';

const PokedexForm = props => (

	<div className="outer-box">
    	<div className="inner-box"> 
        	<div className="text-bold" >Pokémon Lineup</div>
        	<div className="row-box"> 
            	<div className="row-each-box"> 
            		{ 
						props.one && 
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="first" className="data_box"><img src ={props.one} />
						{props.oneName} </button>
						</form>	
					}
            	</div>
            	<div className="row-each-box">
            		{ 
						props.two &&
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="second" className="data_box"><img src ={props.two} />
						{props.twoName} </button>
						</form>
					}
            	 </div>
            	<div className="row-each-box"> 
            		{ 
						props.three &&
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="third" className="data_box"><img src ={props.three} />
						{props.threeName} </button>
						</form>
					}
            	</div>
        	</div>
        	<div className="row-box"> 
            	<div className="row-each-box"> 
            		{ 
						props.four &&
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="fourth" className="data_box"><img src ={props.four} />
						{props.fourName} </button>
						</form>
					}
            	</div>
            	<div className="row-each-box"> 
            		{ 
						props.five &&
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="fifth" className="data_box"><img src ={props.five} />
						{props.fiveName} </button>
						</form>
					}
            	</div>
            	<div className="row-each-box"> 
            		{ 
						props.six &&
						<form onSubmit={props.getDetails}>
						<button  name="slot" value="sixth" className="data_box"><img src ={props.six} />
						{props.sixName} </button>
						</form>
					}
            	</div>
        	</div>
        	<div className="text-bold" >Pokémon Data</div>
        	<div className="row-box-last"> 
        		
        			<div id="divFirst" className="first" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
								<img src ={props.one} />
							<div  className="data-info">Name: 
							<span id ="value-one1" className="value-info"> {props.oneName} </span>
							</div> 
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id ="value-one2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id ="value-one3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id ="value-one4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id ="value-one5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id ="value-one6"className="value-info"> Linkedin/pokemonforever </span></div>

        			</div>
        			<div id="divSecond" className="second" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
									<img src ={props.two} />
							<div  className="data-info">Name: 
							<span id ="value-two1" className="value-info"> {props.twoName} </span>
							</div>
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id = "value-two2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id = "value-two3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id = "value-two4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id = "value-two5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id = "value-two6" className="value-info"> Linkedin/pokemonforever </span></div>
        			</div>
        			<div id="divThird" className="third" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
									<img src ={props.three} />
								<div  className="data-info">Name: 
								<span id ="value-three1" className="value-info"> {props.threeName} </span>
								
							</div> 
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id = "value-three2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id = "value-three3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id = "value-three4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id = "value-three5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id = "value-three6" className="value-info"> Linkedin/pokemonforever </span></div>
        			</div>
        			<div id="divFourth" className="fourth" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
									<img src ={props.four} />
							<div  className="data-info">Name: 
							<span id ="value-four1" className="value-info"> {props.fourName} </span>
							</div> 
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id = "value-four2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id = "value-four3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id = "value-four4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id = "value-four5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id = "value-four6" className="value-info"> Linkedin/pokemonforever </span></div>
        			</div>
        			<div id="divFifth" className="fifth" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
									<img src ={props.five} />
								<div  className="data-info">Name: 
								<span id ="value-five1" className="value-info"> {props.fiveName} </span>
							</div> 
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id = "value-five2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id = "value-five3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id = "value-five4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id = "value-five5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id = "value-five6" className="value-info"> Linkedin/pokemonforever </span></div>
        			</div>
        			<div id="divSixth" className="sixth" style={{display: 'none'}}>
        				{ 
						props.oneName && 
						<div>
							<div class="roww">
								<div className = "bubble"> {props.text} </div> </div>
									<img src ={props.six} />
								<div  className="data-info">Name: 
								<span id ="value-six1" className="value-info"> {props.sixName} </span>
							</div> 
						</div>
						}
						<div  className="data-info">Job Title: 
							<span id = "value-six2" className="value-info"> {props.job} </span></div> 
						<div  className="data-info">Years of experience: 
							<span id = "value-six3" className="value-info"> {props.years} </span></div> 
						<div  className="data-info">Skills: 
							<span id = "value-six4" className="value-info"> {props.skillz} </span></div> 
						<div  className="data-info">Contact: 
							<span id = "value-six5" className="value-info"> pokemonSiAko@gmail.com </span></div> 
						<div  className="data-info">Linkedin: 
							<span id = "value-six6" className="value-info"> Linkedin/pokemonforever </span></div>
        			</div>
        	</div>
    	</div>

    	<div className="inner-box"> 
        	<div className="text-bold" >Pokédex</div>
        	<div className="right-box"> 
				<form onSubmit={props.getPokemon}>
					<input type="text" name="pokemon" placeholder="Pokemon Name" />
					<button className="GeneralButton" >Search Pokemon</button>
				</form>
				<div>
					{ 
						props.sprite && 
						<img src ={props.sprite}/>
					}
					{ 
						props.name && <p className="data">Name: 
							<span className="value"> {props.name} </span>
						</p> 
					}
					{ 
						props.height && <p className="data">Height: 
							<span className="value"> {props.height} </span>
						</p> 
					}
					{ 
						props.types && <p className="data">Type: 
							<span className="value"> {props.types}</span>
						</p> 
					}
					{ 
						props.weight && <p className="data">Weight: 
							<span className="value"> {props.weight}</span>
						</p> 
					}
					{ 
						props.error && <p class="value_error">{props.error}</p> 
					}
					{ 
						props.maxError && <p class="value_error-max">{props.maxError} 
						<button onClick={ props.refreshPage } className="GeneralButton" >Reset</button></p> 
					}
				</div>
				<form onSubmit={props.addPokemon}>
					{ 
						props.sprite && 
						<button className="GeneralButton" >Add Pokemon</button>
					}
				</form>
        	</div>
    	</div>

	</div>
);

export default PokedexForm;