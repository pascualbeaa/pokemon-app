import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'

import './App.css';
import  Pokemon from './App.js';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Pokemon />, document.getElementById('root'));
registerServiceWorker();
