import React from "react"; 

import HeaderText from "./components/HeaderText.js"
import PokedexForm from "./components/PokedexForm.js"

var x = 0;
var spritez;
var neym;
var pokeText = ["", "C'mon make me look goooOood!", "Oh, Hello there! ", "Stuff below better be nice... ayt?", "I wanna be the very best!", "Hello, are you having a great day?", "Are you my trainer?", "Hellooooo, is it me you're looking for?"];
var jobTitle = ["", "Senior QA", "Junior Software Developer", "Intern", "Chill lang", "Mobile Developer", "Meme generator", "Freelancer"];
var yearsExp = ["","0 years","3 years","5 years","10 years","6 Months", "12 years","100 years"];
var skills = ["", "netflix and chill", "c++, c, c#", "Android Programming, Web Programming", "Graphic Design", "Perl, C#, C++, C", "mySql, Restful API, PHP", "Visual Basic, Python"];

class Pokemon extends React.Component {

    state = {
      sprite: undefined,
      name: undefined,
      height: undefined,
      types: undefined,
      weight: undefined,
      error: undefined,
      one: undefined,
      two: undefined,
      three: undefined,
      four: undefined,
      five: undefined,
      six: undefined,
      oneName: undefined,
      twoName: undefined,
      threeName: undefined,
      fourName: undefined,
      fiveName: undefined,
      sixName: undefined,
      maxError: undefined,
      text: undefined,
      job: undefined,
      years: undefined,
      skillz: undefined
    }


  getPokemon = async (e) => {

    e.preventDefault();
    const pokemonName = e.target.elements.pokemon.value.toLowerCase();

    const api_call = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
    const data = await api_call.json();
    

    if(data.name){
      spritez = data.sprites.front_default;
      neym = data.name;

      console.log(data);
      this.setState({
        sprite: data.sprites.front_default,
        name: data.name,
        height: data.height,
        types: data.types[0].type.name,
        weight: data.weight,
        error: ""
      });
    }else{
      this.setState({
        sprite: undefined,
        name: undefined,
        height: undefined,
        types: undefined,
        weight: undefined,
        error: "Please enter the name of the pokemon"
      });
    }
  }

  addPokemon = (e) => {
     e.preventDefault();
     

     x = x + 1;
     
console.log(x);
    switch(x) {
    case 1:
    this.setState({
        one: spritez,
        oneName: neym,
      });
        break;
    case 2:
    this.setState({
        two: spritez,
        twoName: neym
      });
        break;
    case 3:
    this.setState({
        three: spritez,
        threeName: neym
       });
        break;
    case 4:
    this.setState({
        four: spritez,
        fourName: neym
       });
        break;
    case 5:
    this.setState({
        five: spritez,
        fiveName: neym
       });
        break;
    case 6:
    this.setState({
        six: spritez,
        sixName: neym
       });
        break;  
    default:
    this.setState({
        maxError: "Oppss! Lineup is full of amazing creatures, Wanna drop them all!? then Catch em all again??"
       });
}
  }

  getDetails =  (e) => {
    e.preventDefault();
    var randomnumber = Math.floor(Math.random()*7) + 1;
    const slotNumber = e.target.elements.slot.value;

    console.log(slotNumber);

        document.getElementById('divFirst').style.display = "none";
        document.getElementById('divSecond').style.display = "none";
        document.getElementById('divThird').style.display = "none";
        document.getElementById('divFourth').style.display = "none";
        document.getElementById('divFifth').style.display = "none";
        document.getElementById('divSixth').style.display = "none";

        this.setState({
        text: pokeText[randomnumber],
        job: jobTitle[randomnumber], 
        years: yearsExp[randomnumber],
        skillz: skills[randomnumber]
        });
    
    switch(slotNumber) {
    case 'first':
        document.getElementById("value-one1").contentEditable = "true";
        document.getElementById("value-one2").contentEditable = "true";
        document.getElementById("value-one3").contentEditable = "true";
        document.getElementById("value-one4").contentEditable = "true";
        document.getElementById("value-one5").contentEditable = "true";
        document.getElementById("value-one6").contentEditable = "true";
        document.getElementById('divFirst').style.display = "block";
        break;
    case 'second':
        document.getElementById("value-two1").contentEditable = "true";
        document.getElementById("value-two2").contentEditable = "true";
        document.getElementById("value-two3").contentEditable = "true";
        document.getElementById("value-two4").contentEditable = "true";
        document.getElementById("value-two5").contentEditable = "true";
        document.getElementById("value-two6").contentEditable = "true";
        document.getElementById('divSecond').style.display = "block";
        break;
    case 'third':
        document.getElementById("value-three1").contentEditable = "true";
        document.getElementById("value-three2").contentEditable = "true";
        document.getElementById("value-three3").contentEditable = "true";
        document.getElementById("value-three4").contentEditable = "true";
        document.getElementById("value-three5").contentEditable = "true";
        document.getElementById("value-three6").contentEditable = "true";
        document.getElementById('divThird').style.display = "block";
        break;
    case 'fourth':
        document.getElementById("value-four1").contentEditable = "true";
        document.getElementById("value-four2").contentEditable = "true";
        document.getElementById("value-four3").contentEditable = "true";
        document.getElementById("value-four4").contentEditable = "true";
        document.getElementById("value-four5").contentEditable = "true";
        document.getElementById("value-four6").contentEditable = "true";
        document.getElementById('divFourth').style.display = "block";
        break;
    case 'fifth':
        document.getElementById("value-five1").contentEditable = "true";
        document.getElementById("value-five2").contentEditable = "true";
        document.getElementById("value-five3").contentEditable = "true";
        document.getElementById("value-five4").contentEditable = "true";
        document.getElementById("value-five5").contentEditable = "true";
        document.getElementById("value-five6").contentEditable = "true";
        document.getElementById('divFifth').style.display = "block";
        break;
    case 'sixth':
        document.getElementById("value-six1").contentEditable = "true";
        document.getElementById("value-six2").contentEditable = "true";
        document.getElementById("value-six3").contentEditable = "true";
        document.getElementById("value-six4").contentEditable = "true";
        document.getElementById("value-six5").contentEditable = "true";
        document.getElementById("value-six6").contentEditable = "true";
        document.getElementById('divSixth').style.display = "block";
        break;
    
  }

}

  refreshPage = (e) => { 
    window.location.reload(); 
}

  render() {

    return (
        <div>
          <HeaderText />
          <PokedexForm 
            refreshPage={this.refreshPage} 

            getPokemon={this.getPokemon} 
            sprite={this.state.sprite}
            name={this.state.name}
            height={this.state.height}
            types={this.state.types}
            weight={this.state.weight}
            error={this.state.error}

            addPokemon={this.addPokemon} 
            one={this.state.one}
            two={this.state.two}
            three={this.state.three}
            four={this.state.four}
            five={this.state.five}
            six={this.state.six}
            oneName={this.state.oneName}
            twoName={this.state.twoName}
            threeName={this.state.threeName}
            fourName={this.state.fourName}
            fiveName={this.state.fiveName}
            sixName={this.state.sixName}
            maxError={this.state.maxError}
            
            getDetails={this.getDetails} 
            text={this.state.text}
            job={this.state.job}
            years={this.state.years}
            skillz={this.state.skillz}
          />
        </div>
      );
  }
};

export default Pokemon;